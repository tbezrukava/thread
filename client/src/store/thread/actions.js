import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loadPosts = createAsyncThunk(
  ActionType.SET_ALL_POSTS,
  async (filters, { extra: { services } }) => {
    const posts = await services.post.getAllPosts(filters);
    return { posts };
  }
);

const loadMorePosts = createAsyncThunk(
  ActionType.LOAD_MORE_POSTS,
  async (filters, { getState, extra: { services } }) => {
    const {
      posts: { posts }
    } = getState();
    const loadedPosts = await services.post.getAllPosts(filters);
    const filteredPosts = loadedPosts.filter(
      post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
    );

    return { posts: filteredPosts };
  }
);

const applyPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (postId, { extra: { services } }) => {
    const post = await services.post.getPost(postId);
    return { post };
  }
);

const createPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (post, { extra: { services } }) => {
    const { id } = await services.post.addPost(post);
    const newPost = await services.post.getPost(id);

    return { post: newPost };
  }
);

const updatePost = createAsyncThunk(
  ActionType.UPDATE_POST,
  async (payload, { getState, extra: { services } }) => {
    const { id: postId, body } = payload;
    const updatedPost = await services.post.updatePost(postId, { body });
    const mapUpdate = post => ({
      ...post,
      body: updatedPost.body
    });

    const {
      posts: { posts }
    } = getState();
    const updated = posts.map(post => (
      post.id !== postId ? post : mapUpdate(post)
    ));
    return { posts: updated };
  }
);

const applyPostUpdate = createAsyncThunk(
  ActionType.APPLY_POST_UPDATE,
  async (postId, { getState, extra: { services } }) => {
    const updatedPost = await services.post.getPost(postId);
    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (
      post.id !== postId ? post : updatedPost
    ));
    const updatedExpandedPost = expandedPost?.id === postId
      ? updatedPost
      : expandedPost;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const toggleExpandedPost = createAsyncThunk(
  ActionType.SET_EXPANDED_POST,
  async (postId, { extra: { services } }) => {
    const post = postId ? await services.post.getPost(postId) : undefined;
    return { post };
  }
);

const likePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => {
    const { id, createdAt, updatedAt } = await services.post.likePost(postId);
    const likeDiff = id ? 1 : -1;
    let mapReaction;

    if (id) {
      const dislikeDiff = createdAt === updatedAt ? 0 : 1;
      mapReaction = post => ({
        ...post,
        likeCount: Number(post.likeCount) + likeDiff,
        dislikeCount: Number(post.dislikeCount) - dislikeDiff
      });
    } else {
      mapReaction = post => ({
        ...post,
        likeCount: Number(post.likeCount) + likeDiff
      });
    }

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (
      post.id !== postId ? post : mapReaction(post)
    ));
    const updatedExpandedPost = expandedPost?.id === postId
      ? mapReaction(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const dislikePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => {
    const { id, createdAt, updatedAt } = await services.post.dislikePost(postId);
    const dislikeDiff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
    let mapReaction;

    if (id) {
      const likeDiff = createdAt === updatedAt ? 0 : 1;
      mapReaction = post => ({
        ...post,
        likeCount: Number(post.likeCount) - likeDiff,
        dislikeCount: Number(post.dislikeCount) + dislikeDiff
      });
    } else {
      mapReaction = post => ({
        ...post,
        dislikeCount: Number(post.dislikeCount) + dislikeDiff
      });
    }

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (
      post.id !== postId ? post : mapReaction(post)
    ));
    const updatedExpandedPost = expandedPost?.id === postId
      ? mapReaction(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const addComment = createAsyncThunk(
  ActionType.COMMENT,
  async (request, { getState, extra: { services } }) => {
    const { id } = await services.comment.addComment(request);
    const comment = await services.comment.getComment(id);

    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) + 1,
      comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (
      post.id !== comment.postId ? post : mapComments(post)
    ));

    const updatedExpandedPost = expandedPost?.id === comment.postId
      ? mapComments(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const deletePost = createAsyncThunk(
  ActionType.DELETE_POST,
  async (postId, { getState, extra: { services } }) => {
    const deletedPost = await services.post.updatePost(postId, { unusable: true });

    const {
      posts: { posts }
    } = getState();
    const idx = posts.findIndex(post => post.id === deletedPost.id);
    const updated = [
      ...posts.slice(0, idx),
      ...posts.slice(idx + 1)
    ];

    return { posts: updated, expandedPost: undefined };
  }
);

const applyPostDelete = createAsyncThunk(
  ActionType.APPLY_POST_DELETE,
  async (postId, { getState }) => {
    const {
      posts: { posts }
    } = getState();

    const idx = posts.findIndex(post => post.id === postId);
    const updated = [
      ...posts.slice(0, idx),
      ...posts.slice(idx + 1)
    ];

    return { posts: updated };
  }
);

export {
  loadPosts,
  loadMorePosts,
  applyPost,
  applyPostUpdate,
  applyPostDelete,
  createPost,
  updatePost,
  toggleExpandedPost,
  likePost,
  dislikePost,
  addComment,
  deletePost
};
