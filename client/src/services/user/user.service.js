import { HttpMethod, ContentType } from 'common/enums/enums';

class User {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  getUser(id) {
    return this._http.load(`${this._apiPath}/users/${id}`, {
      method: HttpMethod.GET
    });
  }

  updateUser(id, payload) {
    return this._http.load(`${this._apiPath}/users/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  addUserStatus(payload) {
    return this._http.load(`${this._apiPath}/users/statuses`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }
}

export { User };
