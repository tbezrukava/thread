import * as Joi from 'joi';
import {
  UserPayloadKey,
  UserValidationMessage,
  UserValidationRule
} from 'common/enums/enums';

const status = Joi.object({
  [UserPayloadKey.STATUS]: Joi.string()
    .trim()
    .max(UserValidationRule.STATUS_MAX_LENGTH)
    .messages({
      'string.max': UserValidationMessage.STATUS_MAX_LENGTH
    })
});

export { status };
