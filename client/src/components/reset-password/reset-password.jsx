import { useAppForm, useState, useParams } from 'hooks/hooks';
import {
  ButtonType,
  ButtonSize,
  ButtonColor,
  AppRoute,
  IconName,
  UserPayloadKey
} from 'common/enums/enums';
import {
  Button,
  FormInput,
  Message,
  NavLink,
  Segment
} from 'components/common/common';
import { password as passwordValidationSchema } from 'validation-schemas/validation-schemas';
import { auth as authService } from '../../services/services';
import { DEFAULT_RESET_PAYLOAD } from './common/constants';

import styles from './styles.module.scss';

const ResetPassword = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [success, setSuccess] = useState(null);
  const [error, setError] = useState(null);
  const { token } = useParams();
  const { control, errors, handleSubmit } = useAppForm({
    defaultValues: DEFAULT_RESET_PAYLOAD,
    validationSchema: passwordValidationSchema
  });

  const handleReset = values => {
    setIsLoading(true);

    authService.resetPassword({ token, ...values })
      .then(() => setSuccess('The password was successfully reset.'))
      .catch(() => setError('Error. Try to reset your password again.'))
      .finally(setIsLoading(false));
  };

  return (
    <div className={styles.resetContent}>
      {(!success && !error) && (
        <>
          <h2>Set new password</h2>
          <form name="resetForm" onSubmit={handleSubmit(handleReset)}>
            <Segment>
              <fieldset>
                <FormInput
                  name={UserPayloadKey.PASSWORD}
                  type="password"
                  placeholder="Password"
                  iconName={IconName.LOCK}
                  control={control}
                  errors={errors}
                />
                <Button
                  type={ButtonType.SUBMIT}
                  color={ButtonColor.TEAL}
                  size={ButtonSize.LARGE}
                  isLoading={isLoading}
                  isFluid
                  isPrimary
                >
                  Send
                </Button>
              </fieldset>
            </Segment>
          </form>
        </>
      )}
      {success && (
        <Message>
          <span>{success}</span>
          <NavLink to={AppRoute.LOGIN}>Sign In</NavLink>
        </Message>
      )}
      {error && (
        <Message>
          <span>{error}</span>
          <NavLink to={AppRoute.FORGOT}>Forgot password</NavLink>
        </Message>
      )}
    </div>
  );
};

export default ResetPassword;

