import { useAppForm, useState } from 'hooks/hooks';
import {
  ButtonType,
  ButtonSize,
  ButtonColor,
  AppRoute,
  IconName,
  UserPayloadKey
} from 'common/enums/enums';
import {
  Button,
  FormInput,
  Message,
  NavLink,
  Segment
} from 'components/common/common';
import { reset as resetValidationSchema } from 'validation-schemas/validation-schemas';
import { auth as authService } from '../../../../services/services';
import { DEFAULT_RESET_PAYLOAD } from './common/constants';

import styles from './styles.module.scss';

const ForgotForm = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);
  const { control, errors, handleSubmit } = useAppForm({
    defaultValues: DEFAULT_RESET_PAYLOAD,
    validationSchema: resetValidationSchema
  });

  const handleReset = values => {
    setIsLoading(true);
    setError(null);

    authService.forgot(values)
      .then(() => setSuccess('Reset link was sent to your email.'))
      .catch(() => setError('Account doesn\'t exist'))
      .finally(setIsLoading(false));
  };

  return (
    <div>
      {!success ? (
        <>
          <h2 className={styles.title}>Forgot password</h2>
          <form name="resetForm" onSubmit={handleSubmit(handleReset)}>
            <Segment>
              <fieldset className={styles.fieldset}>
                <FormInput
                  name={UserPayloadKey.EMAIL}
                  type="email"
                  placeholder="Email"
                  iconName={IconName.AT}
                  control={control}
                  errors={errors}
                />
                {error && <p className={styles.error}>{error}</p>}
                <Button
                  type={ButtonType.SUBMIT}
                  color={ButtonColor.TEAL}
                  size={ButtonSize.LARGE}
                  isLoading={isLoading}
                  isFluid
                  isPrimary
                >
                  Send link
                </Button>
              </fieldset>
            </Segment>
          </form>
          <Message>
            <span>Back to</span>
            <NavLink to={AppRoute.LOGIN}>Sign In</NavLink>
          </Message>
        </>
      )
        : <p className={styles.success}>{success}</p>}
    </div>
  );
};

export default ForgotForm;
