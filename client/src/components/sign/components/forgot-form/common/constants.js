import { UserPayloadKey } from 'common/enums/enums';

const DEFAULT_RESET_PAYLOAD = {
  [UserPayloadKey.EMAIL]: ''
};

export { DEFAULT_RESET_PAYLOAD };
