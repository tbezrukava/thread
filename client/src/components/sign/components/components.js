import LoginForm from './login-form/login-form';
import RegistrationForm from './registration-form/registration-form';
import ForgotForm from './forgot-form/forgot-form';
import ResetPassword from '../../reset-password/reset-password';

export { LoginForm, RegistrationForm, ForgotForm, ResetPassword };
