import PropTypes from 'prop-types';
import clsx from 'clsx';

import styles from './styles.module.scss';

const TextArea = ({ name, className, onChange, placeholder, rows, value, onBlur }) => (
  <textarea
    className={clsx(styles.textArea, className)}
    name={name}
    onChange={onChange}
    placeholder={placeholder}
    rows={rows}
    value={value}
    onBlur={onBlur}
  />
);

TextArea.propTypes = {
  name: PropTypes.string,
  className: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  rows: PropTypes.number,
  value: PropTypes.string.isRequired,
  onBlur: PropTypes.func
};

TextArea.defaultProps = {
  className: undefined,
  name: '',
  rows: 3,
  placeholder: '',
  onBlur: () => {}
};

export default TextArea;
