import PropTypes from 'prop-types';

import styles from './styles.module.scss';

const ReactedList = ({ reactionType, users }) => {
  const title = `Post was ${reactionType}d by:`;
  let shownUsers = users;
  let otherUsers;
  if (users.length > 11) {
    shownUsers = users.slice(0, 10);
    const diff = users.length - 10;
    otherUsers = `and ${diff} more`;
  }
  const list = shownUsers.map(user => <li key={user.userId}>{user.username}</li>);
  return (
    <div className={styles.reactedList}>
      <p>{title}</p>
      <ul>
        {list}
      </ul>
      {otherUsers && <p>{otherUsers}</p>}
    </div>
  );
};

ReactedList.propTypes = {
  reactionType: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(PropTypes.shape({
    userId: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired
  })).isRequired
};

export default ReactedList;
