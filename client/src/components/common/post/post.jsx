import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { getFromNowTime } from 'helpers/helpers';
import { IconName } from 'common/enums/enums';
import { postType } from 'common/prop-types/prop-types';
import { IconButton, Image } from 'components/common/common';
import { post as postService } from 'services/services';
import { useState } from '../../../hooks/hooks';
import ReactedList from './components/reacted-list/reacted-list';

import styles from './styles.module.scss';

const Post = ({
  post,
  onPostLike,
  onPostDislike,
  onExpandedPostToggle,
  onUpdatePostToggle,
  onPostDelete,
  sharePost
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    userId
  } = post;

  const [users, setUsers] = useState([]);
  const [likePopup, setLikePopup] = useState(false);
  const [dislikePopup, setDislikePopup] = useState(false);
  const date = getFromNowTime(createdAt);
  const { userId: currentUserId } = useSelector(state => ({
    userId: state.profile.user.id
  }));
  const isAuthor = currentUserId === userId;

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleOnReactMouseEnter = (setReacted, reaction) => {
    const counter = reaction === 'isLike' ? likeCount : dislikeCount;
    if (counter > 0) {
      const filter = {
        postId: id,
        reactionType: reaction
      };
      postService.getReacted(filter)
        .then(data => setUsers(data));
      setReacted(true);
    }
  };
  const handleOnReactMouseLeave = setFunction => {
    setFunction(false);
    setUsers([]);
  };
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handleUpdatePostToggle = () => onUpdatePostToggle(id);
  const handlePostDelete = () => onPostDelete(id);

  const handleOnLikeMouseEnter = () => handleOnReactMouseEnter(setLikePopup, 'isLike');
  const handleOnDislikeMouseEnter = () => handleOnReactMouseEnter(setDislikePopup, 'isDislike');
  const handleOnLikeMouseLeave = () => handleOnReactMouseLeave(setLikePopup);
  const handleOnDislikeMouseLeave = () => handleOnReactMouseLeave(setDislikePopup);

  return (
    <div className={styles.card}>
      {image && <Image src={image.link} alt="post image" />}
      <div className={styles.content}>
        <div className={styles.meta}>
          <span>{`posted by ${user.username} - ${date}`}</span>
        </div>
        <p className={styles.description}>{body}</p>
      </div>
      <div className={styles.extra}>
        <div>
          <div className={styles.reactionWrapper}>
            <IconButton
              iconName={IconName.THUMBS_UP}
              label={likeCount}
              onClick={handlePostLike}
              onMouseEnter={handleOnLikeMouseEnter}
              onMouseLeave={handleOnLikeMouseLeave}
            />
            {likePopup && users.length > 0 && (
              <ReactedList
                users={users}
                reactionType="like"
              />
            )}
          </div>
          <div className={styles.reactionWrapper}>
            <IconButton
              iconName={IconName.THUMBS_DOWN}
              label={dislikeCount}
              onClick={handlePostDislike}
              onMouseEnter={handleOnDislikeMouseEnter}
              onMouseLeave={handleOnDislikeMouseLeave}
            />
            {dislikePopup && users.length > 0 && (
              <ReactedList
                users={users}
                reactionType="dislike"
              />
            )}
          </div>
          <IconButton
            iconName={IconName.COMMENT}
            label={commentCount}
            onClick={handleExpandedPostToggle}
          />
          <IconButton
            iconName={IconName.SHARE_ALTERNATE}
            onClick={() => sharePost(id)}
          />
        </div>
        <div hidden={!isAuthor}>
          <IconButton
            iconName={IconName.PEN}
            onClick={handleUpdatePostToggle}
          />
          <IconButton
            iconName={IconName.TRASH}
            onClick={handlePostDelete}
          />
        </div>
      </div>
    </div>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  onUpdatePostToggle: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default Post;
