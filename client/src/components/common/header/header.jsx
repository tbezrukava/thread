import PropTypes from 'prop-types';
import { IconName, IconSize, ButtonType, AppRoute } from 'common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { userType } from 'common/prop-types/prop-types';
import { Button, Icon, Image, NavLink } from 'components/common/common';
import { useCallback, useState, useDispatch, useEffect } from '../../../hooks/hooks';
import { UserValidationRule } from '../../../common/enums/enums';
import { user as userService } from '../../../services/services';

import styles from './styles.module.scss';
import { profileActionCreator } from '../../../store/actions';

const Header = ({ user, onUserLogout }) => {
  const dispatch = useDispatch();
  const [status, setStatus] = useState(user.status?.body || '');

  useEffect(() => {
    setStatus(user.status?.body || '');
  }, [user]);

  const handleUpdateUser = useCallback(
    payload => dispatch(profileActionCreator.updateUser(payload)),
    [dispatch]
  );

  const handleUpdateStatus = data => {
    const payload = {
      id: user.id,
      body: { statusId: data }
    };

    handleUpdateUser(payload);
  };

  const handleStatusOnBlur = async () => {
    if (status !== user.status.body) {
      if (status === '') {
        handleUpdateStatus(null);
      } else {
        const payload = {
          body: status,
          userId: user.id
        };
        userService.addUserStatus(payload)
          .then(data => handleUpdateStatus(data.id));
      }
    }
  };

  const handleStatusOnChange = ev => {
    setStatus(ev.target.value);
  };

  return (
    <div className={styles.headerWrp}>
      {user && (
        <div className={styles.userWrapper}>
          <NavLink to={AppRoute.ROOT}>
            <div>
              <Image
                isCircular
                width="45"
                height="45"
                src={user.image?.link ?? DEFAULT_USER_AVATAR}
                alt="user avatar"
              />
            </div>
          </NavLink>
          <div>
            <NavLink to={AppRoute.ROOT}>
              <div className={styles.userWrapper}>
                {user.username}
              </div>
            </NavLink>
            {user.status && (
              <div className={styles.status}>
                <textarea
                  className={styles.statusInput}
                  maxLength={UserValidationRule.STATUS_MAX_LENGTH}
                  cols={status.length <= 2 ? 1 : status.length - 2}
                  rows={1}
                  value={status}
                  onChange={handleStatusOnChange}
                  onBlur={handleStatusOnBlur}
                />
                <Icon
                  className={styles.icon}
                  name={IconName.PEN}
                />
              </div>
            )}
          </div>
        </div>
      )}
      <div>
        <NavLink to={AppRoute.PROFILE} className={styles.menuBtn}>
          <Icon name={IconName.USER_CIRCLE} size={IconSize.LARGE} />
        </NavLink>
        <Button
          className={`${styles.menuBtn} ${styles.logoutBtn}`}
          onClick={onUserLogout}
          type={ButtonType.BUTTON}
          iconName={IconName.LOG_OUT}
          iconSize={IconSize.LARGE}
          isBasic
        />
      </div>
    </div>
  );
};

Header.propTypes = {
  onUserLogout: PropTypes.func.isRequired,
  user: userType.isRequired
};

export default Header;
