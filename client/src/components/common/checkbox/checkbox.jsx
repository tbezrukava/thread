import PropTypes from 'prop-types';

import styles from './styles.module.scss';

const Checkbox = ({ isChecked, isDisabled, label, onChange }) => (
  <div className={styles.container}>
    <input
      checked={isChecked}
      disabled={isDisabled}
      className={`${styles.switch} ${styles.pointer}`}
      id="toggle-checkbox"
      onChange={onChange}
      type="checkbox"
    />
    <label className={styles.pointer} htmlFor="toggle-checkbox">{label}</label>
  </div>
);

Checkbox.propTypes = {
  isChecked: PropTypes.bool.isRequired,
  isDisabled: PropTypes.bool,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

Checkbox.defaultProps = {
  isDisabled: false
};

export default Checkbox;
