import PropTypes from 'prop-types';
import io from 'socket.io-client';
import {
  NotificationContainer,
  NotificationManager
} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import { useEffect } from 'hooks/hooks';
import { ENV, NotificationMessage, SocketEvent } from 'common/enums/enums';
import { userType } from 'common/prop-types/prop-types';

const socket = io(ENV.SOCKET_URL);

const Notifications = ({ user, onPostApply, onPostUpdateApply, onPostDeleteApply }) => {
  useEffect(() => {
    if (!user) {
      return undefined;
    }
    const { id } = user;
    socket.emit(SocketEvent.CREATE_ROOM, id);
    socket.on(SocketEvent.LIKE, () => {
      NotificationManager.info(NotificationMessage.LIKED_POST);
    });
    socket.on(SocketEvent.DISLIKE, () => {
      NotificationManager.info(NotificationMessage.DISLIKED_POST);
    });
    socket.on(SocketEvent.USERNAME_ERROR, () => {
      NotificationManager.error(NotificationMessage.USERNAME_ERROR);
    });
    socket.on(SocketEvent.NEW_POST, post => {
      if (post.userId !== id) {
        onPostApply(post.id);
      }
    });
    socket.on(SocketEvent.UPDATE_POST, post => {
      if (post.userId !== id) {
        onPostUpdateApply(post.id);
      }
    });
    socket.on(SocketEvent.DELETE_POST, post => {
      if (post.userId !== id) {
        onPostDeleteApply(post.id);
      }
    });

    return () => {
      socket.emit(SocketEvent.LEAVE_ROOM, id);
      socket.removeAllListeners();
    };
  }, [user, onPostApply, onPostUpdateApply, onPostDeleteApply]);

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: userType,
  onPostApply: PropTypes.func.isRequired,
  onPostUpdateApply: PropTypes.func.isRequired,
  onPostDeleteApply: PropTypes.func.isRequired
};

export default Notifications;
