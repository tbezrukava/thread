import PropTypes from 'prop-types';
import { useSelector } from 'hooks/hooks';
import { Spinner, Modal } from 'components/common/common';
import UpdateForm from './components/update-form/update-form';

const UpdatePost = ({ id, handleUpdatePostToggle }) => {
  const { text } = useSelector(state => ({
    text: state.posts.posts.find(it => it.id === id).body
  }));

  const handleUpdatePostClose = () => handleUpdatePostToggle(undefined);

  return (
    <Modal
      isOpen
      onClose={handleUpdatePostClose}
    >
      {text
        ? (
          <UpdateForm
            id={id}
            text={text}
            onUpdate={handleUpdatePostClose}
          />
        )
        : <Spinner />}
    </Modal>
  );
};

UpdatePost.propTypes = {
  id: PropTypes.number.isRequired,
  handleUpdatePostToggle: PropTypes.func.isRequired
};

export default UpdatePost;
