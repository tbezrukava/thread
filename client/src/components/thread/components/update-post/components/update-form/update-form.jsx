import PropTypes from 'prop-types';
import { useCallback, useState } from 'react';
import TextArea from '../../../../../common/text-area/text-area';
import { Button } from '../../../../../common/common';
import { ButtonColor, ButtonType } from '../../../../../../common/enums/enums';

import styles from './styles.module.scss';
import { threadActionCreator } from '../../../../../../store/actions';
import { useDispatch } from '../../../../../../hooks/hooks';

const UpdateForm = ({ id, text, onUpdate }) => {
  const dispatch = useDispatch();
  const [body, setBody] = useState(text);

  const handleUpdatePost = useCallback(payload => (
    dispatch(threadActionCreator.updatePost(payload))
  ), [dispatch]);

  const handleSubmitUpdate = async ev => {
    ev.preventDefault();
    const trimmedBody = body.trim();
    if (!trimmedBody || trimmedBody === text) {
      return;
    }
    handleUpdatePost({ id, body: trimmedBody });
    onUpdate();
  };

  const handleTextChange = useCallback(
    ev => setBody(ev.target.value),
    [setBody]
  );

  return (
    <form className={styles.update} onSubmit={handleSubmitUpdate}>
      <TextArea
        onChange={handleTextChange}
        value={body}
        rows={10}
      />
      <div className={styles.btnWrapper}>
        <Button
          color={ButtonColor.BLUE}
          type={ButtonType.SUBMIT}
        >
          Update
        </Button>
      </div>
    </form>
  );
};

UpdateForm.propTypes = {
  id: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  onUpdate: PropTypes.func.isRequired
};

export default UpdateForm;
