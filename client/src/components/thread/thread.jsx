import {
  useState,
  useCallback,
  useEffect,
  useDispatch,
  useSelector
} from 'hooks/hooks';
import InfiniteScroll from 'react-infinite-scroll-component';
import { threadActionCreator } from 'store/actions';
import { image as imageService } from 'services/services';
import { Post, Spinner, Checkbox } from 'components/common/common';
import { ExpandedPost, SharedPostLink, AddPost, UpdatePost } from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  ownPosts: false,
  likedPosts: false,
  hideOwnPosts: false,
  from: 0,
  count: 10
};

const Thread = () => {
  const dispatch = useDispatch();
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [updatePostId, setUpdatePostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showLikedPosts, setShowLikedPosts] = useState(false);
  const [hideOwnPosts, setHideOwnPosts] = useState(false);
  const [disableOwnPosts, setDisableOwnPosts] = useState(false);
  const [disableHideOwnPosts, setDisableHideOwnPosts] = useState(false);

  const handlePostLike = useCallback(
    id => dispatch(threadActionCreator.likePost(id)),
    [dispatch]
  );

  const handlePostDislike = useCallback(
    id => dispatch(threadActionCreator.dislikePost(id)),
    [dispatch]
  );

  const handleExpandedPostToggle = useCallback(
    id => dispatch(threadActionCreator.toggleExpandedPost(id)),
    [dispatch]
  );

  const handlePostAdd = useCallback(
    postPayload => dispatch(threadActionCreator.createPost(postPayload)),
    [dispatch]
  );

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = useCallback(
    filtersPayload => {
      dispatch(threadActionCreator.loadMorePosts(filtersPayload));
    },
    [dispatch]
  );

  const handlePostDelete = useCallback(
    id => dispatch(threadActionCreator.deletePost(id)),
    [dispatch]
  );

  const handleUpdatePostToggle = id => setUpdatePostId(id);

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    setDisableHideOwnPosts(!disableHideOwnPosts);
    postsFilter.ownPosts = !postsFilter.ownPosts;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    postsFilter.likedPosts = !postsFilter.likedPosts;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleHideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    setDisableOwnPosts(!disableOwnPosts);
    postsFilter.hideOwnPosts = !postsFilter.hideOwnPosts;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = useCallback(() => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  }, [handleMorePostsLoad]);

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  useEffect(() => {
    postsFilter.userId = userId;
  });

  useEffect(() => {
    getMorePosts();
  }, [getMorePosts]);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          isChecked={showOwnPosts}
          isDisabled={disableOwnPosts}
          label="Show only my posts"
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          isChecked={showLikedPosts}
          label="Show liked by me"
          onChange={toggleShowLikedPosts}
        />
        <Checkbox
          isChecked={hideOwnPosts}
          isDisabled={disableHideOwnPosts}
          label="Hide my posts"
          onChange={toggleHideOwnPosts}
        />
      </div>
      <InfiniteScroll
        dataLength={posts.length}
        next={getMorePosts}
        scrollThreshold={0.8}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            onUpdatePostToggle={handleUpdatePostToggle}
            onPostDelete={handlePostDelete}
            sharePost={sharePost}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {updatePostId && (
        <UpdatePost
          handleUpdatePostToggle={handleUpdatePostToggle}
          id={updatePostId}
        />
      )}
      {expandedPost && (
        <ExpandedPost
          sharePost={sharePost}
          setUpdatePostId={setUpdatePostId}
        />
      )}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
