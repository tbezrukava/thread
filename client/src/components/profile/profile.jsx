import { useSelector, useState, useDispatch, useCallback, useEffect } from 'hooks/hooks';
import { Button, Image, Input } from 'components/common/common';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { ImageSize, IconName } from 'common/enums/enums';
import { username as usernameShchema } from '../../validation-schemas/username/username.validation-shchema';
import { status as statusShchema } from '../../validation-schemas/status/status.validation-shchema';
import CropModal from './components/crop-modal/crop-modal';
import { profileActionCreator } from '../../store/actions';
import Icon from '../common/icon/icon';
import { image as imageService, user as userService } from '../../services/services';
import TextArea from '../common/text-area/text-area';

import styles from './styles.module.scss';

const Profile = () => {
  const dispatch = useDispatch();
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));

  const [username, setUsername] = useState(user.username);
  const [status, setStatus] = useState(user.status?.body || '');
  const [usernameError, setUsernameError] = useState(null);
  const [statusError, setStatusError] = useState(null);
  const [uploadImg, setUploadImg] = useState(null);

  useEffect(() => {
    setStatus(user.status?.body || '');
  }, [user]);

  const handleUpdateUser = useCallback(
    payload => dispatch(profileActionCreator.updateUser(payload)),
    [dispatch]
  );

  const handleUpdateAvatar = data => {
    const payload = {
      id: user.id,
      body: { imageId: data }
    };

    handleUpdateUser(payload);
  };

  const handleUpdateStatus = data => {
    const payload = {
      id: user.id,
      body: { statusId: data }
    };

    handleUpdateUser(payload);
  };

  const onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setUploadImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const handleModalClose = () => setUploadImg(null);

  const uploadImage = file => imageService.uploadImage(file);

  const handleUsernameOnChange = ev => {
    setUsernameError(null);
    setUsername(ev.target.value);
  };

  const handleUsernameOnBlur = async () => {
    if (username !== user.username) {
      try {
        await usernameShchema.validateAsync({ username });
        const payload = {
          id: user.id,
          body: { username }
        };
        handleUpdateUser(payload);
      } catch (err) {
        setUsernameError(err.message);
      }
    }
  };

  const handleStatusOnChange = ev => {
    setStatus(ev.target.value);
    setStatusError(null);
  };

  const handleStatusOnBlur = async () => {
    if (status !== user.status?.body) {
      if (status === '') {
        handleUpdateStatus(null);
      } else {
        try {
          await statusShchema.validateAsync({ status });
          const payload = {
            body: status,
            userId: user.id
          };
          userService.addUserStatus(payload)
            .then(data => handleUpdateStatus(data.id));
        } catch (err) {
          setStatusError(err.message);
        }
      }
    }
  };

  return (
    <div className={styles.profile}>
      <div className={styles.imgWrapper}>
        <Image
          alt="profile avatar"
          isCentered
          src={user.image?.link ?? DEFAULT_USER_AVATAR}
          size={ImageSize.MEDIUM}
          isCircular
        />
        <Button
          className={styles.imgBtn}
        >
          <label>
            <Icon
              name={IconName.PEN}
              className={styles.imgIcon}
            />
            <input
              name="image"
              type="file"
              onChange={onSelectFile}
              hidden
            />
          </label>
        </Button>
      </div>
      <div className={styles.inputWrapper}>
        <Input
          iconName={IconName.USER}
          placeholder="Username"
          type="text"
          value={username}
          onChange={handleUsernameOnChange}
          onBlur={handleUsernameOnBlur}
          error={usernameError}
        />
        { usernameError ? (
          <span className={styles.errorMessage}>
            {usernameError}
          </span>
        )
          : null }
      </div>
      <Input
        iconName={IconName.AT}
        placeholder="Email"
        type="email"
        value={user.email}
        disabled
      />
      <div className={styles.inputWrapper}>
        <TextArea
          onChange={handleStatusOnChange}
          placeholder="You can type your status here"
          value={status}
          onBlur={handleStatusOnBlur}
        />
        { statusError ? (
          <span className={styles.errorMessage}>
            {statusError}
          </span>
        )
          : null }
      </div>
      {uploadImg && (
        <CropModal
          img={uploadImg}
          uploadImage={uploadImage}
          handleUpdateAvatar={handleUpdateAvatar}
          handleModalClose={handleModalClose}
        />
      )}
    </div>
  );
};

export default Profile;
