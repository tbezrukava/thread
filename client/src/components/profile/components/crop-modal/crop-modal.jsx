import PropTypes from 'prop-types';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/src/ReactCrop.scss';
import styles from 'components/profile/components/crop-modal/styles.module.scss';
import { useCallback, useRef, useState } from '../../../../hooks/hooks';
import Modal from '../../../common/modal/modal';
import { Button } from '../../../common/common';
import { AvatarSize } from '../../../../common/enums/components/avatar-size.enum';

const CropModal = ({ img, handleModalClose, uploadImage, handleUpdateAvatar }) => {
  const cropProps = {
    aspect: 1,
    unit: '%',
    width: 50,
    x: 25,
    y: 25
  };

  const [crop, setCrop] = useState(cropProps);
  const imgRef = useRef(null);
  const canvasRef = useRef(null);

  const onLoad = useCallback(im => {
    imgRef.current = im;
  }, []);

  const handleOnChange = newCrop => setCrop(newCrop);

  const createCropImg = () => {
    const image = imgRef.current;
    const canvas = canvasRef.current;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      AvatarSize.WIDTH,
      AvatarSize.HEIGHT
    );
  };

  const updateAvatar = () => {
    const canvas = canvasRef.current;
    canvas.toBlob(blob => {
      const file = new File([blob], 'avatar');
      uploadImage(file)
        .then(data => handleUpdateAvatar(data.id));
    });
  };

  const handleOnUpdateClick = () => {
    createCropImg();
    updateAvatar();
    handleModalClose();
  };

  return (
    <Modal
      isOpen
      onClose={handleModalClose}
    >
      <div className={styles.wrapper}>
        <ReactCrop
          src={img}
          onImageLoaded={onLoad}
          crop={crop}
          onChange={handleOnChange}
        />
        <Button
          onClick={handleOnUpdateClick}
        >
          Update
        </Button>
        <canvas
          height={AvatarSize.HEIGHT}
          width={AvatarSize.WIDTH}
          ref={canvasRef}
          hidden
        />
      </div>
    </Modal>
  );
};

CropModal.propTypes = {
  img: PropTypes.string.isRequired,
  handleModalClose: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  handleUpdateAvatar: PropTypes.func.isRequired
};

export default CropModal;
