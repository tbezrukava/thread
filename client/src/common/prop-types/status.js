import PropTypes from 'prop-types';

const statusType = PropTypes.exact({
  id: PropTypes.number.isRequired,
  body: PropTypes.string.isRequired,
  createdAt: PropTypes.string,
  updatedAt: PropTypes.string
});

export { statusType };
