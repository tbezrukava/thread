const IconName = {
  AT: 'at',
  COMMENT: 'comment',
  COPY: 'copy',
  FROWN: 'frown',
  IMAGE: 'image',
  LOCK: 'lock',
  LOG_OUT: 'log out',
  PEN: 'pen',
  SHARE_ALTERNATE: 'share alternate',
  SPINNER: 'spinner',
  THUMBS_UP: 'thumbs up',
  THUMBS_DOWN: 'thumbs down',
  TRASH: 'trash',
  USER: 'user',
  USER_CIRCLE: 'user circle'
};

export { IconName };
