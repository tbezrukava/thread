const AppRoute = {
  ROOT: '/',
  ANY: '*',
  FORGOT: '/forgot',
  LOGIN: '/login',
  REGISTRATION: '/registration',
  RESET_$TOKEN: '/reset/:token',
  PROFILE: '/profile',
  SHARE_$POSTHASH: '/share/:postHash'
};

export { AppRoute };
