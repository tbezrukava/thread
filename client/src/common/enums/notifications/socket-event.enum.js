const SocketEvent = {
  CREATE_ROOM: 'createRoom',
  LEAVE_ROOM: 'leaveRoom',
  LIKE: 'like',
  DISLIKE: 'dislike',
  NEW_POST: 'newPost',
  UPDATE_REACTION: 'updateReaction',
  UPDATE_POST: 'updatePost',
  DELETE_POST: 'deletePost',
  USERNAME_ERROR: 'usernameError'
};

export { SocketEvent };
