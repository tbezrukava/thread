import nodemailer from 'nodemailer';
import { ENV } from '../../common/enums/app/env.enum';

const transporter = nodemailer.createTransport({
  host: ENV.MAILER.HOST,
  port: ENV.MAILER.PORT,
  secure: ENV.MAILER.SECURE,
  auth: {
    user: ENV.MAILER.USER,
    pass: ENV.MAILER.PASS
  }
}, { from: `Thread <${ENV.MAILER.USER}>` });

const mailer = message => transporter.sendMail(message);

export { mailer };
