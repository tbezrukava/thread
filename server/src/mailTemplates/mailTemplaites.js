const createResetMessage = ({ user, url }) => (
  {
    to: user.email,
    subject: 'Reset password',
    html: (
      `
              <p>Dear ${user.username},</p>
              <p>To reset your password click the link below:<br />
              <a href="${url}">${url}</a></p>
            `
    )
  }
);

export { createResetMessage };
