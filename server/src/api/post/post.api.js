import { PostsApiPath } from '../../common/enums/enums';

const initPost = (router, opts, done) => {
  const { post: postService } = opts.services;

  router
    .get(PostsApiPath.ROOT, req => postService.getPosts(req.query))
    .get(PostsApiPath.$ID, req => postService.getPostById(req.params.id))
    .get(PostsApiPath.REACT, req => postService.getReacted(req.query))
    .post(PostsApiPath.ROOT, async req => {
      const post = await postService.create(req.user.id, req.body);
      req.io.emit('newPost', post); // notify all users that a new post was created
      return post;
    })
    .put(PostsApiPath.REACT, async req => {
      const reaction = await postService.setReaction(req.user.id, req.body);

      if (reaction.post && reaction.post.userId !== req.user.id && req.body.isLike) {
        // notify a user if someone (not himself) liked his post
        req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
      }

      if (reaction.post && reaction.post.userId !== req.user.id && req.body.isDislike) {
        // notify a user if someone (not himself) disliked his post
        req.io.to(reaction.post.userId).emit('dislike', 'Your post was disliked!');
      }

      const post = {
        id: req.body.postId,
        userId: req.user.id
      };

      req.io.emit('updatePost', post);

      return reaction;
    })
    .put(PostsApiPath.$ID, async req => {
      const post = await postService.updatePostById(req.params.id, req.body);
      const updatedField = Object.keys(req.body)[0];

      if (updatedField === 'body') {
        req.io.emit('updatePost', post);
      }

      if (updatedField === 'unusable') {
        req.io.emit('deletePost', post);
      }

      return post;
    });

  done();
};

export { initPost };
