import { UsersApiPath } from '../../common/enums/enums';

const initUser = (router, opts, done) => {
  const { user: userService } = opts.services;

  router
    .get(UsersApiPath.$ID, async req => userService.getUserById(req.params.id))
    .post(UsersApiPath.STATUSES, async req => userService.createStatus(req.body))
    .put(UsersApiPath.$ID, async req => {
      if (req.body?.username) {
        const user = await userService.getByUsername(req.body.username);
        if (user) {
          req.io.to(req.user.id).emit('usernameError');
          return user;
        }
      }

      return userService.updateUserById(req.user.id, req.body);
    });

  done();
};

export { initUser };
