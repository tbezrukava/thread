import { CommentsApiPath } from '../../common/enums/enums';

const initComment = (router, opts, done) => {
  const { comment: commentService } = opts.services;

  router
    .get(CommentsApiPath.$ID, req => commentService.getCommentById(req.params.id))
    .post(CommentsApiPath.ROOT, req => {
      const post = {
        id: req.body.postId,
        userId: req.user.id
      };

      req.io.emit('updatePost', post);

      return commentService.create(req.user.id, req.body);
    });

  done();
};

export { initComment };
