import { Abstract } from '../abstract/abstract.repository';

class PostReaction extends Abstract {
  constructor({ postReactionModel }) {
    super(postReactionModel);
  }

  getPostReaction(userId, postId) {
    return this.model.query()
      .select()
      .where({ userId })
      .andWhere({ postId })
      .withGraphFetched('[post]')
      .first();
  }

  getReacted(filter) {
    const { postId, reactionType } = filter;
    return this.model.query()
      .select('userId', 'users.username')
      .where({ postId })
      .andWhere({ [`${reactionType}`]: true })
      .innerJoin('users', 'userId', 'users.id');
  }
}

export { PostReaction };
