const getCommentsCountQuery = model => model.relatedQuery('comments').count().as('commentCount');

const getReactionsQuery = model => isLike => {
  const col = isLike ? 'likeCount' : 'dislikeCount';

  return model.relatedQuery('postReactions')
    .count()
    .where({ isLike })
    .as(col);
};

const getLikedPostsIdQuery = model => userId => (
  model.relatedQuery('postReactions')
    .select('postReactions.postId')
    .where({ userId })
    .andWhere('isLike', true)
);

export { getCommentsCountQuery, getReactionsQuery, getLikedPostsIdQuery };
