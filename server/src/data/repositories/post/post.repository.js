import { Abstract } from '../abstract/abstract.repository';
import { getCommentsCountQuery, getReactionsQuery, getLikedPostsIdQuery } from './helpers';

class Post extends Abstract {
  constructor({ postModel }) {
    super(postModel);
  }

  getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      ownPosts,
      likedPosts,
      hideOwnPosts
    } = filter;

    const posts = this.model.query()
      .select(
        'id',
        'body',
        'createdAt',
        'updatedAt',
        'imageId',
        'userId',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where('unusable', false);

    if (ownPosts === 'true') {
      posts.where({ userId });
    }

    if (likedPosts === 'true') {
      posts.whereIn('id', getLikedPostsIdQuery(this.model)(userId));
    }

    if (hideOwnPosts === 'true') {
      posts.where('userId', '!=', userId);
    }

    return posts
      .withGraphFetched('[image, user.image]')
      .orderBy('createdAt', 'desc')
      .orderBy('id', 'desc')
      .offset(offset)
      .limit(limit);
  }

  getPostById(id) {
    return this.model.query()
      .select(
        'id',
        'body',
        'createdAt',
        'updatedAt',
        'imageId',
        'userId',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where({ id })
      .withGraphFetched('[comments.user.[image, status], user.image, image]')
      .first();
  }
}

export { Post };
