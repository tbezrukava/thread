import { Abstract } from '../abstract/abstract.repository';

class Status extends Abstract {
  constructor({ statusModel }) {
    super(statusModel);
  }
}

export { Status };
