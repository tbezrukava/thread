import { DbTableName } from '../../../common/enums/enums';
import AbstractModel from '../abstract/abstract.model';

class Status extends AbstractModel {
  static get tableName() {
    return DbTableName.STATUSES;
  }

  static get jsonSchema() {
    const baseSchema = super.jsonSchema;

    return {
      type: baseSchema.type,
      required: ['body', 'userId'],
      properties: {
        ...baseSchema.properties,
        body: { type: 'string' },
        userId: { type: ['integer'] }
      }
    };
  }
}

export default Status;
