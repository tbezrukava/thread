const AuthApiPath = {
  LOGIN: '/login',
  REGISTER: '/register',
  FORGOT: '/forgot',
  RESET: '/reset',
  USER: '/user'
};

export { AuthApiPath };
