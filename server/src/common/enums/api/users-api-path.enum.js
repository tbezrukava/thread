const UsersApiPath = {
  ROOT: '/',
  $ID: '/:id',
  STATUSES: '/statuses'
};

export { UsersApiPath };
