const DbTableName = {
  COMMENTS: 'comments',
  IMAGES: 'images',
  POST_REACTIONS: 'postReactions',
  POSTS: 'posts',
  USERS: 'users',
  STATUSES: 'statuses'
};

export { DbTableName };
