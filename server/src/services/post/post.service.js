class Post {
  constructor({ postRepository, postReactionRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    return this._postRepository.getPosts(filter);
  }

  getReacted(filter) {
    return this._postReactionRepository.getReacted(filter);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  updatePostById(id, data) {
    return this._postRepository.updateById(id, data);
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  async setReaction(userId, { postId, isLike, isDislike }) {
    // define the callback for future use as a promise
    const updateOrDelete = react => {
      let result;
      if (react.isLike === isLike && react.isDislike === isDislike) {
        result = this._postReactionRepository.deleteById(react.id);
      } else {
        result = this._postReactionRepository.updateById(react.id, { isLike, isDislike });
      }

      return result;
    };

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._postReactionRepository.create({ userId, postId, isLike, isDislike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._postReactionRepository.getPostReaction(userId, postId);
  }
}

export { Post };
