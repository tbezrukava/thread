class User {
  constructor({ userRepository, statusRepository }) {
    this._userRepository = userRepository;
    this._statusRepository = statusRepository;
  }

  async getUserById(id) {
    const user = await this._userRepository.getUserById(id);

    return user;
  }

  async getByUsername(username) {
    const user = await this._userRepository.getByUsername(username);

    return user;
  }

  async updateUserById(id, data) {
    return this._userRepository.updateById(id, data);
  }

  async createStatus(payload) {
    return this._statusRepository.create(payload);
  }
}

export { User };
